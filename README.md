# XLSXTRACT
Quick and dirty CLI tool for converting `.xlsx` files to JSON.


**Note:** By default (read: not configurable) this tool will convert rich text strings into HTML, removing all attributes (color fontsize, etc) except for `href`s

## Installation
Assuming you have node / npm installed, clone this repo and run `npm i -g`.

You can now use `xlsxtract` anywhere!

## Usage
By default the tool just takes an excel file as input and spits out json to STDOUT:

`xlsxtract input.xlsx`


- If you want to pull from a worksheet that isn't the first one, use the `-w` flag (zero-indexed)
- You can specify which columns you want to pull (by column header) via the `-c` flag
- If you want to output to a file use unix pipes or redirects

`xlsxtract input.xlsx -w 1 -c "ID", "First Name" > output.json`

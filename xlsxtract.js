#! /usr/bin/env node
const { program } = require('commander')
const to_json = require('./to_json')


function extract(file, {columns, workbook}){
    let json = to_json(file, workbook)
    if (columns !== undefined){
        json = json.map(row=>columns.reduce((acc, x)=>({...acc, [x]: row[x]}), {}))
    }
    let out = JSON.stringify(json, null, 4)
    process.stdout.write(out)
}

program
    .usage('<file> (-c column1 column2)')
    .description('Takes an excel file and exports a json list')
    .arguments('<file>')
    .option('-c, --columns <columns...>', "The column header to extract (repeatable)",)
    .option('-w, --workbook <workbook>', "Which workbook index to extract from (defaults to 0)", 0)
    .action(extract)

program.parse()

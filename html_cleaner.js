const { DOMParser, XMLSerializer } = require('@xmldom/xmldom')

function iterClean(element){
    if (element.attributes){
        const attributes = Object.values(element.attributes).filter(x=>x.name !== undefined && x.name !== 'href').map(x=>x.name)
	attributes.forEach(a => {
		element.removeAttribute(a)
	})
    }
    if (element.childNodes){
	    Object.values(element.childNodes).forEach(iterClean)
    }
}

function cleanHtml(instr){
    const dom = new DOMParser().parseFromString('<html><body><div>\n' + instr + '\n<div></body></html>', 'text/html')
    Object.values(dom.childNodes).forEach(iterClean)
    let out = new XMLSerializer().serializeToString(dom).split('\n')
    out.splice(0, 1)
    out.splice(-1, 1)
    return out.join('\n')
}

module.exports = cleanHtml
